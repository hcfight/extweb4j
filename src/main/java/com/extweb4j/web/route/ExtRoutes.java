package com.extweb4j.web.route;

import com.extweb4j.web.controller.DeptController;
import com.extweb4j.web.controller.IconController;
import com.extweb4j.web.controller.IndexController;
import com.extweb4j.web.controller.LogController;
import com.extweb4j.web.controller.MenuController;
import com.extweb4j.web.controller.MsgController;
import com.extweb4j.web.controller.RoleController;
import com.extweb4j.web.controller.UploadController;
import com.extweb4j.web.controller.UserController;
import com.jfinal.config.Routes;

public class ExtRoutes extends Routes{

	@Override
	public void config() {
		// TODO Auto-generated method stub
		this.add("/", IndexController.class);
		this.add("/menu", MenuController.class);
		this.add("/user", UserController.class);
		this.add("/role", RoleController.class);
		this.add("/log", LogController.class);
		this.add("/dept", DeptController.class);
		this.add("/upload", UploadController.class);
		this.add("/icon", IconController.class);
		this.add("/msg", MsgController.class);
	}

}
