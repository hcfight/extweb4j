Ext.define("Admin.model.system.dept.Dept",{
	extend: 'Admin.ux.Model',
    fields: [
        'id',
		'dept_name',
		'dept_desc'
    ]
});